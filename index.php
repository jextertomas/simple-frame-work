<?php
require_once(__DIR__ . "/vendor/autoload.php");

// Set this to display all errors
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Use settings
use SFSettings\SFEnvParser;
$_ENV = (new SFEnvParser('.env'))();

// Init routes
use SFSettings\SFRoutes;
$backendFiles = array_merge(glob('backend/*.php'), glob('backend/*/*.php'), glob('backend/*/*/*.php'));
foreach ($backendFiles as $file) {
    require_once $file;
}

// Run the routes
SFRoutes::run(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
