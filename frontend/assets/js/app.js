function loadContent(page) {
	fetchPage(page);
}

function fetchPage(page) {
	fetch(page)
		.then(response => response.text())
		.then(data => {
			document.getElementById("app").innerHTML = data;
			executeScripts(data);
		})
		.catch(error => {
			console.error("Error fetching page:", error);
			document.getElementById("app").innerHTML =
				"<h2>Error Loading Page</h2>";
		});
}

function executeScripts(html) {
	const tempElement = document.createElement("div");
	tempElement.innerHTML = html;

	const scripts = tempElement.querySelectorAll("script");

	scripts.forEach(script => {
		const newScript = document.createElement("script");
		newScript.textContent = script.textContent;
		document.body.appendChild(newScript).parentNode.removeChild(newScript);
	});
}
