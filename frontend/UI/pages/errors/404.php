
<?php UIHelper::start('title', 'Error 404'); ?>
<div id="notfound">
    <div class="notfound">
        <div class="notfound-404">
            <h1>4<span style="background-image: url('<?= asset('errorAssets/img/emoji.png') ?>');"></span>4</h1>
        </div>
        <h2>Oops! Nothing To Look Here! Page Could Not Be Found!</h2>
        <p>Sorry but the page you are looking for does not exist, have been removed. name changed or is temporarily unavailable</p>
        <a href="<?= route('dashboard') ?>">Back to homepage</a>
    </div>
</div>
<?php UIHelper::end(); ?>
<?php UIHelper::uses(layout('error')); ?> 