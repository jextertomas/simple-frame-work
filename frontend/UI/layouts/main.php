<!--
=========================================================
* Material Dashboard 2 - v3.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard
* Copyright 2023 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= asset('assets/img/apple-icon.png') ?>">
        <link rel="icon" type="image/png" href="<?= asset('assets/img/favicon.png') ?>">
        <title><?= UIHelper()->getTitle(); ?></title>
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
        <!-- Nucleo Icons -->
        <link href="<?= asset('assets/css/nucleo-icons.css') ?>" rel="stylesheet" />
        <link href="<?= asset('assets/css/nucleo-svg.css') ?>" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <!-- Material Icons -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
        <!-- CSS Files -->
        <link id="pagestyle" href="<?= asset('assets/css/material-dashboard.css?v=3.1.0') ?>" rel="stylesheet" />
        <!-- Nepcha Analytics (nepcha.com) -->
        <!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
        <script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    </head>

    <body class="g-sidenav-show  bg-gray-200">
        <?= UIHelper::uses(aside('aside')); ?>
        <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
            <?= UIHelper::uses(aside('navbar')); ?>
            <div class="container-fluid py-4">
                <?= UIHelper()->getContent(); ?>
                <?= UIHelper::uses(aside('footer')); ?>
            </div>
        </main>
        <?= UIHelper::uses(aside('settings')); ?>
        <!--   Core JS Files   -->
        <script src="<?= asset('assets/js/core/popper.min.js') ?>"></script>
        <script src="<?= asset('assets/js/core/bootstrap.min.js') ?>"></script>
        <script src="<?= asset('assets/js/plugins/perfect-scrollbar.min.js') ?>"></script>
        <script src="<?= asset('assets/js/plugins/smooth-scrollbar.min.js') ?>"></script>
        <script src="<?= asset('assets/js/plugins/chartjs.min.js') ?>"></script>
        <script>
            var win = navigator.platform.indexOf('Win') > -1;
            if (win && document.querySelector('#sidenav-scrollbar')) {
                var options = {
                    damping: '0.5'
                }
                Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
            }
        </script>
        <!-- Github buttons -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="<?= asset('assets/js/material-dashboard.min.js?v=3.1.0') ?>"></script>
    </body>

</html>