<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="emoji" sizes="76x76" href="<?= asset('errorAssets/img/emoji.png') ?>">
    <link rel="icon" type="image/png" href="<?= asset('errorAssets/img/emoji.png') ?>">
	<title><?= UIHelper()->getTitle(); ?></title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,700" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="<?=  asset('errorAssets/css/style.css') ?>" />

</head>
<body>
    <?= UIHelper()->getContent(); ?>
</body>

</html>