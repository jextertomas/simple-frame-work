<?php
namespace SFSettings;

use SFSettings\SFPDO;

class SFQuery extends SFPDO{
    private static $query = '';
    private static $params = [];

    // Starting a transaction
    public static function beginTransaction(){
        return parent::getConnection()->beginTransaction();
    }
    public static function commit(){
        return parent::getConnection()->commit();
    }
    public static function rollback(){
        return parent::getConnection()->rollback();
    }



    // Data Retrieving
    public static function select($columns = []){
        self::$query .= "SELECT " .implode(", ", $columns). " ";
        return new self();
    }
    public static function from($table){
        self::$query .= "FROM $table ";
        return new self();
    }


    // Data Manipulations
    // Inserting
    public static function insertInto($table, $columns = []) {
        self::$query .= "INSERT INTO $table (".implode(", ", $columns).") ";
        return new self();
    }
    public static function values($values = []) {
        $keys = array();
        foreach ($values as $key) {
            $keys[] = "?";
            self::$params[] = $key;
        }
        self::$query .= "VALUES (".implode(", ", $keys).") ";
        return new self();
    }

    // Updating
    public static function update($table){
        self::$query .= "UPDATE $table ";
        return new self();
    }
    public static function set($pairs = []){
        $keys = array();

        foreach ($pairs as $key => $value) {
            $keys[] = "$key = ?";
            self::$params[] = $value;
        }
        self::$query .= "SET ".implode(", ", $keys)." ";
        return new self();
    }

    // Deleting
    public static function deleteFrom($table){
        self::$query .= "DELETE FROM $table ";
        return new self();
    }

    // Conditions and filtering
    public static function where($condition){
        self::$query .= "WHERE $condition ";
        return new self();
    }
    // Operators
    public static function equal($value){
        self::$query .= "LIKE %$value% ";
        return new self();
    }
    public static function notEqual($value){
        self::$query .= "LIKE %$value% ";
        return new self();
    }
    public static function greaterThan($value){
        self::$query .= "LIKE %$value% ";
        return new self();
    }
    public static function greaterThanEqual($value){
        self::$query .= "LIKE %$value% ";
        return new self();
    }
    public static function lessThan($value){
        self::$query .= "LIKE %$value% ";
        return new self();
    }
    public static function lessThenEqual($value){
        self::$query .= "LIKE %$value% ";
        return new self();
    }
    public static function like($value){
        self::$query .= "LIKE %$value% ";
        return new self();
    }

    public static function and($condition){
        self::$query .= "AND $condition ";
        return new self();
    }
    public static function or($condition){
        self::$query .= "OR $condition ";
        return new self();
    }
    public static function between(){
        self::$query .= "WHERE $column ";
        return new self();
    }

    // View query as string
    public static function toString(){
        header('Content-Type: application/json');
        echo json_encode(['query' => self::$query, 'params' => self::$params], JSON_PRETTY_PRINT);
        exit;
    }   
    // Execute query
    public static function execute() {
        try {
            $statement = parent::getConnection()->prepare(self::$query);
            $statement->execute(self::$params);
            if ($statement->rowCount() > 0) {
                return $statement;
            }else{
                header('Content-Type: application/json');
                echo json_encode(['status' => 'error'], JSON_PRETTY_PRINT);
                exit;
            }
        } catch (\PDOException $e) {
            header('Content-Type: application/json');
            echo json_encode(['status' => 'error', 'error' => $e->getMessage()], JSON_PRETTY_PRINT);
            exit;
        }
        
    }
}