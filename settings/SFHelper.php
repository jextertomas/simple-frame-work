<?php
use SFSettings\SFPDO;

// Return types in backend
function usePage($file, $args = []){ // Can use in backend codes if you want to return a page instead of json
    ob_start();
    extract($args);
    require_once($_SERVER['DOCUMENT_ROOT'] . $_ENV['APP_BASE_URL']."/frontend/UI/pages/".$file.".php");
    exit;
}
function useJson($args, $status = 200){
    header('Content-Type: application/json');
    http_response_code($status);
    echo json_encode($args, JSON_PRETTY_PRINT);
    exit;
}

// Class Helper
class UIHelper {
    private static $data = [
        
    ];

    public function __call($name, $arguments) {
        $prefix = 'get';
        if (strpos($name, $prefix) === 0) {
            $property = lcfirst(substr($name, strlen($prefix)));
            if (array_key_exists($property, self::$data)) {
                return self::$data[$property];
            }
        }
        throw new Exception("Method $name not found");
    }

    public function __get($name) {
        if (array_key_exists($name, self::$data)) {
            return self::$data[$name];
        }
        throw new Exception("Property $name not found");
    }

    public static function start($key, $value){
        ob_start();
        self::$data[$key] = $value;
    }

    public static function end(){
        $res = ob_get_clean();
        self::$data['content'] = $res;
        return $res;
    }

    public static function uses($file, $content = '<h3>SimpleFramework</h3>', $title = 'SimpleFramework'){ // Use to use above functions
        require_once($_SERVER['DOCUMENT_ROOT'] . $file);
    }
}
function UIHelper(){
    return new UIHelper();
}

// Return UI paths helper
function jsAsset($file){ // Path to js assets
    return $_ENV['APP_BASE_URL']."/frontend/assets/js/".$file.".js";
}
function cssAsset($file){ // Path to css assets
    return $_ENV['APP_BASE_URL']."/frontend/assets/css/".$file.".css";
}
function pages($file){ // Path to pages
    return $_ENV['APP_BASE_URL']."/frontend/UI/pages/".$file.".php";
}
function aside($file){ // Path to asides
    return $_ENV['APP_BASE_URL']."/frontend/UI/asides/".$file.".php";
}
function layout($file = 'main'){ // Layout to use
    return $_ENV['APP_BASE_URL']."/frontend/UI/layouts/".$file.".php";
}
function asset($file){
    return $_ENV['APP_BASE_URL']."/frontend/assets/".$file;
}
function route($path){
    return $_ENV['APP_BASE_URL']."/".$path;
}