<?php
namespace SFSettings;

class SFPDO{
    private static $pdo;

    public static function getConnection() {
        $host = $_ENV['DB_HOST'];
        $dbname = $_ENV['DB_NAME'];
        $username = $_ENV['DB_USER'];
        $password = $_ENV['DB_PASS'];
        try {
            self::$pdo = new \PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            self::$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            self::$pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            self::$pdo->setAttribute(\PDO::ATTR_TIMEOUT, 30);
            return self::$pdo;
        } catch (\PDOException $e) {
            // Handling connection errors
            echo "Connection failed: " . $e->getMessage();
            return null;
            exit;
        }
    }
}