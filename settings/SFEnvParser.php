<?php

namespace SFSettings;

class SFEnvParser
{
    private $FILE_PATH;
    private $envVars = [];

    public function __construct($path)
    {
        $this->FILE_PATH = $path;
        if (file_exists($this->FILE_PATH)) {
            $lines = file($this->FILE_PATH, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

            foreach ($lines as $line) {
                if (strpos($line, '=') !== false && strpos($line, '#') !== 0) {
                    list($key, $value) = explode('=', $line, 2);
                    $this->envVars[trim($key)] = trim($value);
                }
            }
        }
    }
    public function __invoke()
    {
        return $this->envVars;
    }
}
