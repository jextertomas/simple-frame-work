<?php
namespace SFSettings;

class SFRoutes {
    public static $routes = [];
    
    public static function route($methods, $path, $handler) {
        $methodList = explode('|', $methods);
        $parsedPath = preg_replace('/:(\w+)/', '([^\/]+)', $path);
        $newPath = str_replace("([^\/]+)" , "PARAM", $parsedPath);
        $quoted = preg_quote($newPath, '/');
        $parsedPath = str_replace("PARAM" , "([^\/]+)", $quoted);
        foreach ($methodList as $method) {
            self::$routes[$method][$_ENV['APP_BASE_URL'] . '' . $parsedPath] = [
                'handler' => $handler,
                'params' => self::extractParams($path)
            ];
        }
    }

    public static function run($uri) {
        $method = $_SERVER['REQUEST_METHOD'];
        $handler = null;
        $params = [];

        if (strpos($uri, $_ENV['APP_BASE_URL'] . '/frontend') === 0 || strpos($uri, $_ENV['APP_BASE_URL'] . '/backend') === 0 || strpos($uri, $_ENV['APP_BASE_URL'] . '/settings') === 0) {
            http_response_code(404);
            require_once($_SERVER['DOCUMENT_ROOT'] . $_ENV['APP_BASE_URL'] . "/frontend/UI/pages/errors/404.php");
            exit;
        }

        foreach (self::$routes[$method] as $route => $info) {
            if (preg_match('/^\\' . $route . '$/', $uri, $matches)) {
                $handler = $info['handler'];
                array_shift($matches);
                $params = array_combine($info['params'], $matches);
                break;
            }
        }
        
        
        if ($handler) {
            $json = json_decode(file_get_contents("php://input"), true);
            $request = new Request($method, $uri, $_REQUEST, $_POST, $json, $params);
            call_user_func($handler, $request);
            exit;
        } else {
            http_response_code(404);
            require_once($_SERVER['DOCUMENT_ROOT'] . $_ENV['APP_BASE_URL']."/frontend/UI/pages/errors/404.php");
            exit;
        }
    }

    private static function extractParams($path) {
        preg_match_all('/\/:(\w+)/', $path, $matches);
        return $matches[1];
    }
}

// For request
class Request {
    private $method;
    private $uri;
    private $params;
    private $body;
    private $jsonBody;
    private $urlParams;

    public function __construct($method, $uri, $params, $body, $jsonBody, $urlParams) {
        $this->method = $method;
        $this->uri = $uri;
        $this->params = $params;
        $this->body = ($method == 'GET') ? null:$body;
        $this->jsonBody = ($method == 'GET') ? null:$jsonBody;
        $this->urlParams = $urlParams;
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }
}